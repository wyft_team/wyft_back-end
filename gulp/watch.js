'use strict';

let gulp = require('gulp');
let $ = require('./getPlugins');

gulp.task('watch', ['nodemon']);

gulp.task('nodemon', () => {
  return $.nodemon({
    script: 'server.js',
    tasks: ['lint:node']
  });
});
