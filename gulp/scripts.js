'use strict';

let gulp = require('gulp');
let browserify = require('browserify');
let config = require('./config');
let path = require('path');
let source = require('vinyl-source-stream');
let stylish = require('jshint-stylish');
let babelify = require('babelify');
let $ = require('./getPlugins');

// lint application code
gulp.task('lint', () => {
  return gulp.src(path.join(config.paths.src, '**/*.js'))
      .pipe($.jshint())
      .pipe($.jshint.reporter(stylish));
});

gulp.task('lint:node', () => {
  return gulp.src(
    [
      path.join(config.paths.routes, '**/*.js'),
      path.join(config.paths.controllers, '**/*.js'),
      path.join(config.paths.libs, '**/*.js'),
      path.join(config.paths.models, '**/*.js')
    ])
      .pipe($.jshint())
      .pipe($.jshint.reporter(stylish));
});

gulp.task('browserify', ['lint'], () => {
  return browserify(path.join(config.paths.src, '/app.module.js'))
        .transform("babelify", { presets: ['es2015', 'stage-0'] })
        .bundle()
        .pipe(source('app.js'))
        .pipe($.vinylBuffer())
        .pipe($.minify().on('error', $.util.log))
        .pipe($.uglify({mangle: false}).on('error', $.util.log))
        .pipe($.sourcemaps.init({loadMaps: true}))
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(path.join(config.paths.tmp, 'app')));
});
