'use strict';
let passport = require('passport');
let passport_local = require('passport-local');
let Members = require('../../../models/members');

const LocalStrategy = passport_local.Strategy;

const strategy = new LocalStrategy('local_members',
  { usernameField: 'email', passwordField: 'hash'},
  ( email, password, done ) => {
    
    Members.findOne({ email }, (err, user) => {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorect email.' });
      }
      if (!user.validatePassword(password)) {
        return done(null, false, { message: 'Incorect password.' });
      }
      return done(null, user);
    });
});

module.exports = strategy;