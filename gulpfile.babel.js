'use strict';

let gulp = require('gulp');
let reqDir = require('requiredir');
let path = require('path');

reqDir(path.join(__dirname, './gulp'), { recurse: true });

/* Default task that runs build process */
gulp.task('default', ['watch'], () => {
  //nothing here
});
