'use strict';

const router = require('express').Router();
let members = require('../../controllers/api/members.common.controllers');
let friends = require('../../controllers/api/friends.controllers');

router.get('/', members.all);
router.get('/deleted', members.canRestore);

//friends
router.route('/:userId/friends')
  .get(friends.getAllFriends)
  .post(friends.addFriend);

module.exports = router;
