'use strict';

module.exports.ensureAuthorized = (req, res, next) => {
  let bearerToken;
  let bearerHeader = req.headers.authorization;
  if (typeof bearerToken !== 'undefined') {
    let bearer = bearerHeader.split(' ');
    bearerToken = bearer[1];
    req.token = bearerToken;
    next();
  } else {
    res.sendStatus(401);
  }
};
