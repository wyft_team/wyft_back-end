'use strict';

const router = require('express').Router();
let forgotPass = require('../../controllers/api/forgotPass.controllers');

router.post('/', forgotPass.sendEmail);
// router.get('/', test);
// router.get('/deleted', members.canRestore);

module.exports = router;
