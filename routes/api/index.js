'use strict';

const router = require('express').Router();
let validation = require('./bootstrap');
let auth = require('./auth');
let members = require('./members');
let { ensureAuthorized } = require('./permissions');
let forgotPass = require('./forgotPass');
let newPass = require('../../controllers/api/newPass.controllers');
  
// router.use(ensureAuthorized);

/* Pre built answer */
router.use( validation.validateAccepHeaders );
router.use( validation.setHeaders );
router.use( validation.setMetaInformation );
router.use( validation.setApiVersion );
router.use( validation.setLinks );
router.use( validation.constructJsonResponse );

//authorization and authentication
router.use( '/auth', auth );
router.use( '/forgot_pass', forgotPass );

router.post('/new_pass', newPass.confirm);

//members routes
//TODO: restore ensureAuthorized middleware
router.use( '/members', members );
module.exports = router;
