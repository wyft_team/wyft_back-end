'use strict';

const router = require('express').Router();
let auth = require('../../../controllers/api/auth.controllers');

router.route('/sign_in').post(auth.signInMember);
router.route('/sign_up').post(auth.signUpMember);

module.exports = router;
