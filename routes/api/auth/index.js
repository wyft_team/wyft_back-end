'use strict';

const router = require('express').Router();
let member = require('./member');
let common = require('./common');

router.use('/', common);
router.use('/members', member);

module.exports = router;
