'use strict';

let Members = require('../../models/members.model');

let jwt = require('jsonwebtoken');
let cf = require('konfig')();

module.exports.signInMember = (req, res, next) => {
  let response = res.locals.predefinedResponse;

  let { email, hash } = req.body || {};
  Members.authMember( email, hash, promise => {
    promise.then(
      member => {
        console.log(member);
        response.data = member;
        res.json(response);
      },
       error => {
        response.error = error.message;
        res.status(404).json( response );
      });
  });
};

module.exports.signUpMember = ( req, res, next ) => {
  let response = res.locals.predefinedResponse;

  //get credetinals
  let { personal_info } = req.body;

  Members
    .findOne()
    .where('personal_info.email').eq(personal_info.email)
    .exec((err, member) => {
      if (err) return next(err);
      if (member) {
        let error = {
          message: 'Member already exists'
        };

        response.error = error;
        response.data = null;
        res.sendStatus(404);
      } else {
        Members.hashPassword(personal_info.hash).then(hashed => {
          personal_info.hash = hashed;

          Members
            .create( req.body , (err, createdMember) => {
              if (err || !createdMember) return next(err);

              let token = jwt.sign( createdMember, cf.app.secret_key );
              createdMember.token = token;
              createdMember.save( (err, ack) => {
                if (err) return next(err);
                if (ack) {
                  //delete sensitive fields
                  ack = ack.toObject();
                  delete ack.hash;

                  response.data = ack;
                  res.json( response );
                }
              });
            });
          }, err => { return next( err ); });
      }
    });
};
