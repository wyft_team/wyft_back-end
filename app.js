'use strict'

/* let modules */
let express = require('express');
let logger = require('morgan');
let path = require('path');
let bodyParser = require('body-parser');
let cookieParser = require('cookie-parser');
let favicon = require('serve-favicon');
let passport = require('passport');
let flash = require('connect-flash');
let helmet = require('helmet');

/* let libs */
let mongoose = require('./libs/storages/mongoose');

/* let routes */
let router = require('./routes');

let app = express();

//preventions
app.use(helmet());

app.use(logger('dev'));
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
app.use(cookieParser());
app.use(passport.initialize());

/* favicon */
// app.use(favicon(path.join(__dirname, '.tmp/images/favicon/favicon.ico')));

/* initialize third-party modules */
mongoose.init();

/* apply routes */
app.use('/', router);

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use( (err, req, res, next) => {
    let response = res.locals.predefinedResponse;
    response.error = Object.assign(err);
    response.data = null;
    res.status(404).json( response );
  });
}

// production error handler
// no stacktraces leaked to user
// app.use((err, req, res, next) => {
//   res.json( err );
// });

app.use( (err, req, res, next) => {
  let response = res.locals.predefinedResponse;
  response.error = Object.assign(err);
  response.data = null;
  res.status(404).json( response );
});

module.exports = app;
