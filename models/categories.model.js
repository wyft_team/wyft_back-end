'use strict';

let mongoose = require('mongoose');

// options for connection using mongodb driver
let options = {
  autoIndex: true,
  toJSON: { getters: true, virtuals: true },
  bufferCommands: true,
  id: false,
  minimize: false,
  safe: { j:1, w: 1, wtimeout: 5000 }, //we want to ensure that all were saved
  strict: false
};

const categorySchema = new mongoose.Schema({
  title : { type: String, minlength: 1, maxlength: 50, required: true }
}, options);

module.exports = mongoose.Model( 'Category', categorySchema );
