'use strict';
let bcrypt = require('bcrypt-nodejs');

module.exports = exports = (hash) => {
  const saltRounds = 10;
  const password = hash;
  return new Promise( (resolve, reject) => bcrypt.hash( password, null, null, (err, hash) => {
      if ( err ) reject( err );
      resolve( hash );
    })
  );
};
