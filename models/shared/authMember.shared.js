'use strict';
let bcrypt = require('bcrypt-nodejs');

module.exports = exports = function (email, password, cb) {
  return this.findOne({ 'personal_info.email': email }).select('personal_info').exec( (err, member) => {
    if (err) throw new Error(err);
    if (member) {
      let promise = new Promise( (resolve, reject) => {
        let result = bcrypt.compareSync(password, member.personal_info.hash);
        if (result) {
          //delete sensitive information
          member = member.toObject();
          delete member.personal_info.hash;

          resolve(member);
        } else reject( new Error('Incorrect credentials') );
      });

      cb( promise );
    } else {
      cb( Promise.reject(false) );
    }
  });
};
