'use strict';

let mongoose = require('mongoose');

// options for connection using mongodb driver
let options = {
  autoIndex: true,
  toJSON: { getters: true, virtuals: true },
  bufferCommands: true,
  id: false,
  minimize: false,
  safe: { j:1, w: 1, wtimeout: 5000 }, //we want to ensure that all were saved
  strict: false
};

// enum with acceptable codes
// 0 - friend request
// 1 - new message recieved
//TODO: in future array will be filled out
const types = [ 0, 1, 2, 3 ];

//TODO: negotiate notification schema
const notificationSchema = new mongoose.Schema({
  type : { type: Number, enum: types, required: true }
}, options);
