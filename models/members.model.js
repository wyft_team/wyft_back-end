'use strict';

let mongoose = require('mongoose');
let authMember = require('./shared/authMember.shared');
let hashPassword = require('./shared/hashPassword.shared');
let statics = require('./members.statics');

const emailRegExp = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

// options for connection using mongodb driver
let options = {
  autoIndex: true,
  toJSON: { getters: true, virtuals: true },
  bufferCommands: true,
  id: false,
  minimize: false,
  safe: { j:1, w: 1, wtimeout: 5000 }, //we want to ensure that all were saved
  strict: false
};

const personal_info = {
  first_name : { type: String, minlength: 2, maxlength: 35, required: true, index: true },
  last_name : { type: String, minlength: 2, maxlength: 35, required: true, index: true },
  age : { type: Number, minlength: 16, maxlength: 100, required: false },
  profile_photo: { type: String, minlength: 10, maxlength: 500, required: false, default: '/here/put/path/to/default/photo' },
  email: { type: String, required: true, match: emailRegExp, index: true },
  hash: { type: String, required: true, select: false, minlength: 6 }
};

const social_accounts = {
  first_name : { type: String, minlength: 2, maxlength: 35, required: true },
  last_name : { type: String, minlength: 2, maxlength: 35, required: true },
  avatar_link: { type: String, minlength: 10, maxlength: 500, required: true },
  token : { type: String, required: true, select: false, minlength: 30 },
  s_profile_link : { type: String, minlength: 10, maxlength: 300, required: true },
  // we have here additinal email field per each social account doc 'cause user in each
  // social network can use different email address
  email: { type: String, required: true, match: emailRegExp, index: true }
};

const reviews = {
  global_rating : { type: Number, minlength: 0.0, maxlength: 5.0, required: true, default: 0.0 },
  reviews : { ref: 'Review', type: mongoose.Schema.Types.ObjectId }
};

//TODO: experiment with timestamps for created_at and updated_at for schema!
const geo = {
  //TODO: think about setting some default coors
  latt : { type: Number, required: true },
  long : { type: Number, required: true },
  updated_at : { type: Date, required: true, default: Date.now() }
};

const preferences = {
  categories : [ { ref: 'Category', type: mongoose.Schema.Types.ObjectId } ],
  // TODO: decide either will be using kilometers or meters (1.2km vs 1200m )
  radius : { type: Number, minlength: 0, maxlength: 25, required: true, default: 1 }
};

const memberSchema = new mongoose.Schema({
  personal_info : personal_info,
  social_accounts : [ social_accounts ],
  reviews : reviews,
  friends : [ { ref: 'Member', type: mongoose.Schema.Types.ObjectId } ],
  geo : geo,
  preferences : preferences,
  is_deleted: { type: Boolean, required: true, default: false },
  // TODO: think about required
  token : { type: String, required: false }
  //TODO: think about friend requests

}, options);

memberSchema.virtual('displayName')
 .get(function() {
    return `${this.firstname} ${this.lastname}`;
 })
 .set(function(name) {
    let fullName = name.split(' ');
    this.firstname = fullName[0];
    this.lastname = fullName[1];
 });

//TODO: somehow move these statics as below
//member schema statics for convenient using and reusability
memberSchema.statics.hashPassword = hashPassword;
memberSchema.statics.authMember = authMember;

//now we do like that
memberSchema.static( statics );

module.exports = mongoose.model('Member', memberSchema);
