'use strict';

let mongoose = require('mongoose');
//TODO: put here member model if face errors related to refs

// options for connection using mongodb driver
let options = {
  autoIndex: true,
  toJSON: { getters: true, virtuals: true },
  bufferCommands: true,
  id: false,
  minimize: false,
  safe: { j:1, w: 1, wtimeout: 5000 }, //we want to ensure that all were saved
  strict: false
};

const reviewsSchema = new mongoose.Schema({
  reviewer : { ref: 'Member', type: mongoose.Schema.Types.ObjectId },
  created_at : { type: Date, required: true, default: Date.now() },
  rating : { type: Number, required: true, minlength: 0.0, maxlength: 5.0 },
  review_title : {type : String, required: true, minlength: 5, maxlength: 100},
  review_text : { type: String, minlength: 10, maxlength: 1000, required: true },
  for_whom : { ref: 'Member', type: mongoose.Schema.Types.ObjectId  }
}, options);

module.exports = mongoose.Model('Review', reviewsSchema);
